import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url: string = 'https://big-money-pruebas.herokuapp.com/';
  cryptoPassword: string = "Maindsoft1314";

  constructor(public http: HttpClient) { }

  login(data){
    return this.http.post(this.url + 'user/login', data);
  }

  getPicks(data){
    return this.http.post(this.url + 'user/getPicks', data);
  }

  createPicks(data){
    return this.http.post(this.url + 'user/insertPicks', data);
  }

  deletePicks(data){
    return this.http.post(this.url + 'user/deletePicks', data);
  }

  getPick(data){
    return this.http.post(this.url + 'user/getSinglePick', data);
  }

  updatePicks(data){
    return this.http.post(this.url + 'user/updatePicks', data);
  }

  sendNotificacionTema(data){
    return this.http.post(this.url + 'notificaciones/tema', data);
  }

  getPuestos(){
    return this.http.post(this.url + 'admin/getPuestos', '');
  }

  getUsers(){
    return this.http.post(this.url + 'admin/getUsers', '');
  }

  getDeletedUsers(){
    return this.http.post(this.url + 'admin/getDeletedUsers', '');
  }

  getUser(data){
    return this.http.post(this.url + 'admin/getUser', data);
  }

  deleteUsers(data){
    return this.http.post(this.url + 'admin/deleteUsers', data);
  }

  register(data){
    return this.http.post(this.url + 'user/register', data);
  }

  updateUser(data){
    return this.http.post(this.url + 'user/updateUser', data);
  }

  getCountry(){
    return this.http.post(this.url + 'user/getCountry', '');
  }

  getMembresiasAnio(){
    return this.http.post(this.url + 'admin/getMembershipYear', '');
  }

  getMembresias(){
    return this.http.post(this.url + 'user/getMembership', '');
  }

  getMetodosPagos(){
    return this.http.post(this.url + 'admin/getMetodosPagos', '');
  }

  getMembresiasMes(data){
    return this.http.post(this.url + 'admin/getMembershipPeriod', data);
  }

  getMembershipHistory(data){
    return this.http.post(this.url + 'admin/getMembershipHistory', data);
  }

  insertMembership(data){
    return this.http.post(this.url + 'user/insertMembership', data);
  }

  getNotice(){
    return this.http.post(this.url + 'getNotice', '');
  }

  CreateNews(data){
    return this.http.post(this.url + 'create-new', data);
  }

  UpdateNews(data){
    return this.http.post(this.url + 'update-new', data);
  }

  DeleteNews(data){
    return this.http.post(this.url + 'delete-new', data);
  }
}
