import * as moment from 'moment';
import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

declare var $: any;

function dateVaidator(AC: AbstractControl) {
  if (AC && AC.value && !moment(AC.value, 'YYYY-MM-DD',true).isValid()) {
    return {'dateVaidator': true};
  }

  let fecha1 = moment(AC.value);
  let fecha2 = moment(new Date());
  let diff = fecha1.diff(fecha2, 'days');
  
  if (diff < 0) {
    return {'dateVaidator': true};
  }

  return {'dateVaidator': false};
}

@Component({
  selector: 'app-new-picks',
  templateUrl: './new-picks.component.html',
  styleUrls: ['./new-picks.component.css']
})

export class NewPicksComponent implements OnInit {

  picksForm: FormGroup;
  id: number;

  text_button: string;
  pick_name: string;
  load: boolean = true;

  constructor(private api: ApiService,
              private router: Router,
              public formBuilder: FormBuilder,
              private routeActive: ActivatedRoute) {
    this.picksForm = this.createMyForm();
    this.id = this.routeActive.snapshot.params['id_picks'];
  }

  private createMyForm(){
    return this.formBuilder.group({
      TITULO: ['', [Validators.required, Validators.maxLength(100)]],
      DESCRIPCION: ['', [Validators.required]],
      FECHA: ['', [Validators.required, dateVaidator]]
    });
  }

  ngOnInit(): void {
    //Date range picker
    this.load = false;
    this.text_button = 'Save'
    this.pick_name = 'New Pick'

    if (this.id != null) {
      this.llenarCampos(this.id);
    }
  }

  llenarCampos(id){
    this.load = true;
    this.api.getPick({ID_PICKS: id}).subscribe(result => {
      let data = result['data'];
      if (data.response == 1) {
        this.pick_name = 'UPDATE ' + data.data.TITULO;

        this.picksForm.controls.TITULO.setValue(data.data.TITULO);
        this.picksForm.controls.DESCRIPCION.setValue(data.data.DESCRIPCION.replace(/<[^>]*>?/g, '\n'));
        let fecha = formatDate(data.data.FECHA, 'yyyy-MM-dd', 'en');
        this.picksForm.controls.FECHA.setValue(fecha);
      }
      this.load = false;
    })
  }

  saveData(){
    let data = this.picksForm.value;
    this.load = true;

    data.DESCRIPCION = data.DESCRIPCION.replace('\'', "\\'").replace(/\n/g, "<br>");

    if (this.id) {
      this. updatePick();
    }else{
      this.guardar(data);
    }
  }

  guardar(data){
    this.api.createPicks(data).subscribe(result =>{
      if (result['data'].response == 1) {
        
        let fecha1 = moment(data.FECHA);
        let fecha2 = moment(new Date());
        let diff = fecha1.diff(fecha2, 'days');

        if (diff > 0) {
          this.load = false;

          this.router.navigate(['/picks']);
        }else{
          let message = {
            TITLE: 'NEW PLAY ALERT!',
            BODY: data.TITULO
          }

          this.api.sendNotificacionTema(message).subscribe(data => {
            this.load = false;

            this.router.navigate(['/picks']);
          });
        }

      }else{
        
      }
    },  err => {

    });
  }

  updatePick(){
    let data = this.picksForm.value;

    data.DESCRIPCION = data.DESCRIPCION.replace('\'', "\\'").replace(/\n/g, "<br>");
    data.ID = this.id;

    this.api.updatePicks(data).subscribe(result =>{
      if (result['data'].response == 1) {
        
        let fecha1 = moment(result['data'].data[0].FECHA);
        let fecha2 = moment(new Date());
        let diff = fecha1.diff(fecha2, 'days');

        if (diff > 0) {
          this.load = false;

          this.router.navigate(['/picks']);
        }else{
          let message = {
            TITLE: 'New pick',
            BODY: data.TITULO
          }

          this.api.sendNotificacionTema(message).subscribe(data => {
            this.load = false;

            this.router.navigate(['/picks']);
          });
        }
        
      }else{
        
      }
    });
  }

}
