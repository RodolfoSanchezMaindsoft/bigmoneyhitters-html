import * as moment from 'moment';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-picks',
  templateUrl: './picks.component.html',
  styleUrls: ['./picks.component.css']
})

export class PicksComponent implements OnDestroy, OnInit {

  picks: any;
  dtOptions: DataTables.Settings = {};
  tipo: number;
  datetime: any;
  load: boolean = true;

  dtTrigger: any = new Subject();

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.initPicks();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  initPicks(){

    this.load = true;

    this.api.getPicks({ORIGEN: 'ADMIN'}).subscribe(result => {
      let res = result['data'];
      if (res.response == 1) {
        this.picks = res.data;
        this.dtTrigger.next();
      }
      this.load = false;
    });
  }

  deletePick(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.confirmDelete(id);
      }
    })
  }
  

  confirmDelete(id){
    this.load = true;

    this.api.deletePicks({ID: id}).subscribe(data => {
      let result = data['data'];
      if(result.response == 1){
        this.refresh();
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
      this.load = false;
    });
  }

  editPick(id){
    this.router.navigate(['/picks/update-picks', id]);
  }

  getFecha(fecha){
    return  moment(fecha).add('days', 1).format('YYYY-MM-DD');
  }

  refresh(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/picks']);
    });
  }

}
