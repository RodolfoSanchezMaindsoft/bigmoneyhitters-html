import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PicksRoutingModule } from './picks-routing.module';
import { PicksComponent } from './picks.component';
import { NewPicksComponent } from './new-picks/new-picks.component';

import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PicksComponent, NewPicksComponent],
  imports: [
    FormsModule,
    CommonModule,
    DataTablesModule,
    PicksRoutingModule,
    ReactiveFormsModule
  ]
})
export class PicksModule { }
