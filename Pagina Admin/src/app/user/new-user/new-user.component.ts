import Swal from 'sweetalert2';
import * as CryptoJS from 'crypto-js';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  userForm: FormGroup;
  id_user: number;
  text_button: any;
  load: boolean = true;
  userName: string;
  
  paises: any;
  membresias: any;
  metodos_pago: any;

  constructor(private api: ApiService,
            private router: Router,
            public formBuilder: FormBuilder,
            private routeActive: ActivatedRoute) { }

  ngOnInit(): void {
    this.id_user = this.routeActive.snapshot.params['id'];
    if (this.id_user) {
      this.text_button = 'Update';
      this.userForm = this.createUpdateUser();
      this.getCountry();
      this.getUser();
    }else{
      this.userName = 'New user'
      this.text_button = 'Save';
      this.userForm = this.createNewUser();
      this.getCountry();
    }

  }

  private createNewUser(){
    return this.formBuilder.group({
      NOMBRE: ['', [Validators.required]],
      TELEFONO: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
      EMAIL: ['', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]],
      PASSWORD: ['', [Validators.required, Validators.minLength(6)]],
      CIUDAD: ['', [Validators.required]],
      PAIS: ['', [Validators.required]],
      TERMINOS: [1, [Validators.required]],
      ID_SUSCRIPTION: ['', [Validators.required]],
      PAYMENT: ['', [Validators.required]]
    });
  }

  private createUpdateUser(){
    return this.formBuilder.group({
      ID_USER: [this.id_user, [Validators.required]], 
      NOMBRE: ['', [Validators.required]],
      TELEFONO: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
      EMAIL: ['', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]],
      CIUDAD: ['', [Validators.required]],
      PAIS: ['', [Validators.required]]
    });
  }

  getCountry(){
    this.api.getCountry().subscribe(result => {
      let resultado = result['data'];
      
      if (resultado.response == 1) {
        this.paises = resultado.data;
      }

      this.getMemberships();
    }, err => {
      this.load = false;
    });
  }

  getMemberships(){
    this.api.getMembresias().subscribe(res => {
      let resultado = res['data'];
      
      if (resultado.response == 1) {
        this.membresias = resultado.data;
      }

      this.getPaymenths();
    }, err => {
      this.load = false;
    });
  }

  getPaymenths(){
    this.api.getMetodosPagos().subscribe(res => {
      let resultado = res['data'];
      
      if (resultado.response == 1) {
        this.metodos_pago = resultado.data;
      }

      this.load = false;
    }, err => {
      this.load = false;
    });
  }

  getUser(){
    this.api.getUser({id: this.id_user}).subscribe(res => {
      let respuesta = res['data'];
      if (respuesta.response == 1) {
        let user = respuesta.data;
        
        this.userName = user.NOMBRE;
        this.userForm.controls.NOMBRE.setValue(user.NOMBRE);
        this.userForm.controls.TELEFONO.setValue(user.TELEFONO);
        this.userForm.controls.EMAIL.setValue(user.CORREO);
        this.userForm.controls.CIUDAD.setValue(user.CIUDAD);
        this.userForm.controls.PAIS.setValue(user.ID_PAIS);
      }
    });
  }

  saveData(){
    this.load = true;
    if (this.id_user) {
      this.updateUser();
    }else{
      this.createUser();
      console.log('save');
    }
  }

  createUser(){
    let data = this.userForm.value;

    data.PASSWORD = CryptoJS.AES.encrypt(data.PASSWORD.trim(), this.api.cryptoPassword).toString();

    this.api.register(data).subscribe(result => {
      let resultado = result['data'];
      
      if (resultado.response == 1 && resultado.data[0].CLIENTE != 'El usuario ya existe') {
        if (
            (this.userForm.value.PAYMENT && this.userForm.value.PAYMENT != '') && 
            (this.userForm.value.ID_SUSCRIPTION && this.userForm.value.ID_SUSCRIPTION != '')
        ) {
          this.insertMembresia(resultado.data[0].ID_CLIENTE, this.userForm.value.PAYMENT)
        }else{
          this.router.navigate(['/users']);
        }
      } else{
        Swal.fire({
          icon: 'error',
          title: 'The user is exist',
          showConfirmButton: false,
          timer: 1500
        });
      }

      this.load = false;
    }, err => {
      this.load = false;
    });
  }

  insertMembresia(id, metodo){
    this.api.insertMembership({
      ID_CLIENTE: id,
      ID_MEMBRESIA: this.userForm.value.ID_SUSCRIPTION,
      ID_PAGO: 1,
      METODO_PAGO: metodo
    }).subscribe(result => {
      if (result['data'].response == 1) {
        this.router.navigate(['/users']);
      }
    });
  }

  updateUser(){
    let data = this.userForm.value;

    data.ID_USER = this.id_user;

    this.api.updateUser(data).subscribe(result => {
      let resultado = result['data'];

      if (resultado.response == 1) {

        Swal.fire({
          icon: 'success',
          title: 'The user has been updated',
          showConfirmButton: false,
          timer: 1500
        });

        this.router.navigate(['/users']);
      }

      this.load = false;
    }, err =>{
      this.load = false;
    });

  }

}
