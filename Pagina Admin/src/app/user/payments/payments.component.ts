import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {

  id_user: number;
  history: any;

  constructor(private api: ApiService,
              private router: Router,
              private routeActive: ActivatedRoute) { }

  ngOnInit(): void {
    this.id_user = this.routeActive.snapshot.params['id'];

    this.initHistory();
  }

  initHistory(){
    let id = this.id_user ? this.id_user : 0
    this.api.getMembershipHistory({ id_user: id }).subscribe(res => {
      let respuesta = res['data'];
      if (respuesta.response == 1) {
        this.history = respuesta.data;
      }
    });
  }

  getFecha(fecha){
    return  moment(fecha).add('days', 1).format('MMMM Do YYYY');
  }
}
