import * as moment from 'moment';
import { Chart } from 'chart.js';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

declare var $: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  pieChart: Chart = [];
  pieChartMes:Chart = [];
  labels = [];
  labelsMes = [];

  load: any = {
    anual: true,
    periodo: true
  }

  fechas = {
    inicio: moment().format('YYYY-MM')+'-01',
    fin: moment().format('YYYY-MM-DD')
  }

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.initMembresias();
    this.initPeriod();
  }

  initMembresias(){
    this.load.anual = true;
    
    this.api.getMembresiasAnio().subscribe(res => {
      let respuesta = res['data'];
      if (respuesta.response == 1) {
        this.labels =  this.groupBy(respuesta.data);
        setTimeout(() => {
          this.initGrafica();
          this.load.anual = false;
        }, 30);
      }
    });
  }

  groupBy (xs) {

    let newArray = [
      {
        id: 1,
        label: 'One Day Subscription',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        backgroundColor: [
          'rgba(181, 120, 19, 0.2)'
        ],
        borderColor: [
            'rgba(181, 120, 19, 1)'
        ],
        borderWidth: 1
      },
      {
        id: 2,
        label: 'One Week Subscription',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        backgroundColor: [
          'rgba(137 , 0, 137, 0.2)'
        ],
        borderColor: [
            'rgba(137 , 0, 137, 1)'
        ],
        borderWidth: 1
      },
      {
        id: 3,
        label: 'One Month Subscription',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        backgroundColor: [
          'rgba(0, 140, 48, 0.2)'
        ],
        borderColor: [
            'rgba(0, 140, 48, 1)'
        ],
        borderWidth: 1
      }
    ];

    newArray.map(membresia => {
      for (let index = 0; index < xs.length; index++) {
        const element = xs[index];
        
        if (element.ID_MEMBRESIA == membresia.id) {
          membresia.data[element.MES - 1] = element.TOTAL;
        }
      }

    });

    return newArray;
  };


  initGrafica(){
    this.pieChart = new Chart ('pieChartAnual', {
      type: 'line',
      data: {
          labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          datasets: this.labels
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
    });
  }

  initGraficaMes(){
    this.pieChartMes = new Chart ('pieChartPeriod', {
      type: 'bar',
      data: {
          labels: ['Suscriptions'],
          datasets: this.labelsMes
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
    });
  }

  initPeriod(){
    this.load.periodo = true;
    let datos = [];

    this.api.getMembresiasMes(this.fechas).subscribe(res => {
      let respuesta = res['data'];
      if (respuesta.response == 1) {
        datos = respuesta.data;
        let label = [];
        let dataset = [];
        datos.map(element => {
          label.push(element.NOMBRE_MEMBRESIA_INGLES);
          dataset.push({
            label: element.NOMBRE_MEMBRESIA_INGLES,
            data: [element.TOTAL],
            backgroundColor: [
                'rgba('+element.COLOR+', 0.2)',
            ],
            borderColor: [
                'rgba('+element.COLOR+', 1)',
            ],
            borderWidth: 1
          })
        });
        
        this.labelsMes = dataset;
        this.initGraficaMes();
        this.load.periodo = false;
      }
    })
  }

  validDates(fecha){
    return moment(fecha, 'YYYY-MM-DD',true).isValid()
  }
}
