import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.css']
})
export class CreateNewsComponent implements OnInit {

  text_button: string = 'guardar';
  notice_name: string= 'Create news'

  NoticeForm: FormGroup;

  load: boolean = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '20em',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      // ['bold', 'italic'],
      ['insertImage', 'insertVideo']
    ]
};

  constructor(private api: ApiService,
              private router: Router,
              public formBuilder: FormBuilder,
              private routeActive: ActivatedRoute) {
                this.NoticeForm = this.createMyForm();
               }

  ngOnInit(): void {
  }

  private createMyForm(){
    return this.formBuilder.group({
      TITULO: ['', [Validators.required, Validators.maxLength(150)]],
      IMAGEN: ['', [Validators.required]],
      CONTENIDO: ['', [Validators.required]]
    });
  }

  saveData(){
    let data = this.NoticeForm.value;
    
    this.api.CreateNews(data).subscribe(res => {
      if (res['data'].response == 1) {
        this.router.navigate(['/news']);
      }else{
        Swal.fire(
          'Error!',
          'an error has occurred while uploading the news.',
          'error'
        )
      }
    }, err => {
      Swal.fire(
        'Error!',
        'an error has occurred while uploading the news.',
        'error'
      )
    });
    
  }

}
