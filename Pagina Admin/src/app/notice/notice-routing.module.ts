import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoticeComponent } from './notice.component';
import { CreateNewsComponent } from './create-news/create-news.component';

const routes: Routes = [
  { path: '', component: NoticeComponent },
  { path: 'create-news', component: CreateNewsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoticeRoutingModule { }
