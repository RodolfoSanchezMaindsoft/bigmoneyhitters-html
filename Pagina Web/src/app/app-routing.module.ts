import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginUserGuard } from './guards/login-user.guard';
import { UserGuard } from './guards/user.guard';
import { MemberGuard } from './guards/member.guard';


const routes: Routes = [
  { path: '', loadChildren: () => import('./index/index.module').then(m => m.IndexModule), canActivate: [UserGuard] },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule), canActivate: [UserGuard] },
  { path: 'register', loadChildren: () => import('./register/register.module').then(m => m.RegisterModule), canActivate: [UserGuard] },
  { path: 'picks', loadChildren: () => import('./picks/picks.module').then(m => m.PicksModule), canActivate: [MemberGuard] },
  { path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule), canActivate: [LoginUserGuard] },
  { path: 'support', loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule), canActivate: [LoginUserGuard] },
  { path: 'frecuency-questions', loadChildren: () => import('./faq/faq.module').then(m => m.FaqModule) },
  { path: 'membership', loadChildren: () => import('./membership/membership.module').then(m => m.MembershipModule), canActivate: [MemberGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
