import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url: string = 'https://big-money-pruebas.herokuapp.com/';
  cryptoPassword: string = "Maindsoft1314";

  constructor(public http: HttpClient) { }

  login(data){
    return this.http.post(this.url + 'user/login', data);
  }

  register(data){
    return this.http.post(this.url + 'user/register', data);
  }

  getCountry(){
    return this.http.post(this.url + 'user/getCountry', '');
  }

  getPicks(data){
    return this.http.post(this.url + 'user/getPicks', data);
  }

  createPicks(data){
    return this.http.post(this.url + 'user/insertPicks', data);
  }

  deletePicks(data){
    return this.http.post(this.url + 'user/deletePicks', data);
  }

  getPick(data){
    return this.http.post(this.url + 'user/getSinglePick', data);
  }

  updatePicks(data){
    return this.http.post(this.url + 'user/updatePicks', data);
  }

  getMembership(){
    return this.http.post(this.url + 'user/getMembership', '');
  }
  
  insertMembership(data){
    return this.http.post(this.url + 'user/insertMembership', data);
  }
  
  ChangePassword(data){
    return this.http.post(this.url + 'user/changePassword', data);
  }

  updateUser(data){
    return this.http.post(this.url + 'user/updateUser', data);
  }

  sendNotificacionTema(data){
    return this.http.post(this.url + 'notificaciones/tema', data);
  }

  getUserMembership(data){
    return this.http.post(this.url + 'getUserMembership', data);
  }
}
