import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MemberGuard implements CanActivate {

  constructor(private menu: MenuService, private router:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.menu.usuario) {
        if (this.menu.usuario.VENCIMIENTO && this.menu.usuario.VENCIMIENTO >= 0) {
          return true;
        }else{
          return this.router.parseUrl("/membership");
        }
      } else {
        return this.router.parseUrl("/login");
      }
  }
  
}
