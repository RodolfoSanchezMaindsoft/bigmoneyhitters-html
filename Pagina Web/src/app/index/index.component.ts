import { MenuService } from '../services/menu.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private menu: MenuService, private location: Location) {
  }

  ngOnInit(): void {
    this.menu.pagina = this.location.path();
  }

}
