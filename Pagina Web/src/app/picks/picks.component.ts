import { Location } from '@angular/common';
import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { MenuService } from '../services/menu.service';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-picks',
  templateUrl: './picks.component.html',
  styleUrls: ['./picks.component.css']
})
export class PicksComponent implements OnInit {

  datetime: any;
  picks: any;

  constructor(private menu: MenuService, private location: Location, private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.menu.usuario.VENCIMIENTO;
    this.menu.pagina = this.location.path();
    $('#loading').show();
    this.initPicks();
    this.getDate();
  }

  getDate(){
      this.datetime = formatDate(new Date(), 'MMMM dd yyyy', 'en');
  }

  initPicks(){
    this.api.getPicks({ORIGEN: 'USER'}).subscribe(result => {
      let res = result['data'];
      if (res.response == 1) {
        this.picks = res.data;
      }
      $('#loading').hide();
    },err => {
      $('#loading').hide();
    });
  }

  formatDatePick(fecha){
    let new_fecha = fecha.split('T')[0];
    return formatDate(new_fecha, 'yyyy/MM/dd', 'en');
  }

  formatMonthPick(fecha){
    return formatDate(fecha, 'MMMM', 'en');
  }

}
